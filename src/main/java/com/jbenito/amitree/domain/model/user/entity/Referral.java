package com.jbenito.amitree.domain.model.user.entity;

import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;
import lombok.Value;

@Value(staticConstructor = "of")
public class Referral {
    private final EmailAddress email;
    private final ReferralCode code;
}
