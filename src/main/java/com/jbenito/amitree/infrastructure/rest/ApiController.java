package com.jbenito.amitree.infrastructure.rest;

import com.jbenito.amitree.application.user.create.CreateUser;
import com.jbenito.amitree.application.user.create.CreateUserCommand;
import com.jbenito.amitree.application.user.get.GetUser;
import com.jbenito.amitree.domain.model.user.value.UserId;
import com.jbenito.amitree.infrastructure.logging.Loggable;
import com.jbenito.amitree.infrastructure.rest.dto.UserResponse;
import lombok.NonNull;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import static com.jbenito.amitree.infrastructure.rest.dto.UserResponse.fromUser;

@RestController
public class ApiController {

    private CreateUser createUser;
    private GetUser getUser;

    public ApiController(CreateUser createUser, GetUser getUser) {
        this.createUser = createUser;
        this.getUser = getUser;
    }

    @PostMapping("/user")
    @ResponseStatus(HttpStatus.CREATED)
    @Loggable(operation = "create-user", successMessage = "User created")
    UserResponse createUser(@RequestBody CreateUserCommand command) {
        return fromUser(createUser.execute(command));
    }

    @GetMapping("/user/{id}")
    @Loggable(operation = "get-user", successMessage = "User fetched")
    UserResponse getUser(@NonNull @PathVariable("id") String userId) {
        return fromUser(getUser.execute(UserId.of(userId)));
    }

}
