package com.jbenito.amitree.unit.infrastructure.data;

import com.jbenito.amitree.domain.model.user.entity.Referral;
import com.jbenito.amitree.domain.model.user.entity.ReferralRepository;
import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;
import com.jbenito.amitree.infrastructure.data.InMemoryReferralRepository;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

class InMemoryReferralRepositoryTest {

    private ReferralRepository referralRepository = new InMemoryReferralRepository();

    @Test
    void givenReferral_whenSaved_thenReferralIsAddedToRepository() {
        Referral referral = anyReferral();
        referralRepository.save(referral);

        Referral retrievedReferral = referralRepository.findByEmailAndCode(referral.getEmail(), referral.getCode())
                .orElseThrow(() -> new RuntimeException("Referral not found"));

        assertThat(retrievedReferral).isNotNull();
        assertThat(referral).isEqualTo(retrievedReferral);
    }

    @Test
    void givenNonExistentReferral_whenFindBy_thenNoReferralIsReturned() {
        Referral savedReferral = anyReferral(), nonSavedReferral = anyReferral();
        referralRepository.save(savedReferral);

        Optional<Referral> retrievedReferral = referralRepository.findByEmailAndCode(nonSavedReferral.getEmail(), nonSavedReferral.getCode());

        assertThat(retrievedReferral).isEmpty();
    }

    @Test
    void givenRepositoryWithMultipleReferrals_whenFindBy_thenTheRightReferralIsReturned() {
        Referral targetReferral = anyReferral(), anotherReferral = anyReferral("another.user@amitree.com");
        referralRepository.save(targetReferral);
        referralRepository.save(anotherReferral);

        Referral referral = referralRepository.findByEmailAndCode(targetReferral.getEmail(), targetReferral.getCode())
                .orElseThrow(() -> new RuntimeException("Referral not found"));

        assertThat(referral).isEqualTo(targetReferral);
    }

    private Referral anyReferral(String email) {
        return Referral.of(EmailAddress.of(email), ReferralCode.create());
    }

    private Referral anyReferral() {
        return anyReferral("john.doe@amitree.com");
    }
}