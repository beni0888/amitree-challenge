package com.jbenito.amitree.application.user.get;

import com.jbenito.amitree.domain.model.user.entity.User;
import com.jbenito.amitree.domain.model.user.entity.UserRepository;
import com.jbenito.amitree.domain.model.user.error.UserNotFound;
import com.jbenito.amitree.domain.model.user.value.UserId;

public class GetUser {
    private UserRepository userRepository;

    public GetUser(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User execute(UserId userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UserNotFound(userId));
    }
}
