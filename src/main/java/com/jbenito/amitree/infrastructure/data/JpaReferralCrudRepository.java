package com.jbenito.amitree.infrastructure.data;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JpaReferralCrudRepository extends JpaRepository<JpaReferral, String> {
    Optional<JpaReferral> findOneByEmailAndCode(String email, String code);
}
