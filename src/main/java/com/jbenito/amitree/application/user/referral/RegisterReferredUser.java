package com.jbenito.amitree.application.user.referral;

import com.jbenito.amitree.domain.model.user.entity.Referral;
import com.jbenito.amitree.domain.model.user.entity.ReferralRepository;
import com.jbenito.amitree.domain.model.user.entity.User;
import com.jbenito.amitree.domain.model.user.entity.UserRepository;
import com.jbenito.amitree.domain.model.user.error.NonExistentReferralCode;
import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;

public class RegisterReferredUser {

    private final ReferralRepository referralRepository;
    private final UserRepository userRepository;

    public RegisterReferredUser(ReferralRepository referralRepository, UserRepository userRepository) {

        this.referralRepository = referralRepository;
        this.userRepository = userRepository;
    }

    public void execute(RegisterReferredUserCommand command) {
        ReferralCode referralCode = ReferralCode.of(command.referralCode);
        EmailAddress referredUserEmail = EmailAddress.of(command.email);
        User user = userRepository.findByReferralCode(referralCode)
                .orElseThrow(() -> new NonExistentReferralCode(referralCode));
        user.addReferralReward();
        userRepository.save(user);
        referralRepository.save(Referral.of(referredUserEmail, referralCode));
    }
}
