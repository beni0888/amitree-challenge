package com.jbenito.amitree.domain.model.user.service;

import java.util.Base64;
import java.util.UUID;

public final class UniqueCodeGenerator {

    public static final String get() {
        byte[] uniqueCode = UUID.randomUUID().toString().getBytes();
        return Base64.getEncoder().encodeToString(uniqueCode);
    }
}
