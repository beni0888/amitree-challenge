package com.jbenito.amitree.domain.model.user.value;

import lombok.Value;

@Value
public class Credit {

    public static final Credit ZERO = Credit.of(0);

    private final int value;

    public static Credit of(int value) {
        return new Credit(value);
    }

    public Credit add(final Credit credit) {
        return Credit.of(value + credit.value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Credit credit = (Credit) o;
        return value == credit.value;
    }
}
