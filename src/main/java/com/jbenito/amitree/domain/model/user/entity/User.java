package com.jbenito.amitree.domain.model.user.entity;

import com.jbenito.amitree.domain.model.user.value.*;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Data(staticConstructor = "of")
@Builder
@Slf4j
public class User {
    public static final Credit REWARD_CREDIT = Credit.of(10);
    private static final RewardsCounter REWARD_THRESHOLD = RewardsCounter.of(5);

    @NonNull
    private final UserId id;
    @NonNull
    private final EmailAddress email;
    @NonNull
    private Credit credit;
    @NonNull
    private RewardsCounter rewardsCounter;
    @NonNull
    @Builder.Default
    private final ReferralCode referralCode = ReferralCode.create();

    public static User of(@NonNull User anotherUser) {
        return new User(UserId.of(anotherUser.id.getValue().toString()),
                EmailAddress.of(anotherUser.email.getValue()),
                Credit.of(anotherUser.credit.getValue()),
                RewardsCounter.of(anotherUser.rewardsCounter.getValue()),
                ReferralCode.of(anotherUser.referralCode.getValue()));
    }

    public void addCredit(@NonNull Credit credit) {
        this.credit = this.credit.add(credit);
        log.info("message=\"Added credit to user: [ id: {}, email: {}, credit: {}]\"", id, email, credit);
    }

    public void addReferralReward() {
        rewardsCounter = rewardsCounter.increment();

        if (rewardsCounter.isGreaterThanOrEqual(REWARD_THRESHOLD)) {
            log.info("message=\"User has reached reward threshold, extra credit will be granted: [ id: {}, email: {}, credit: {}]\"", id, email, credit);
            rewardsCounter = rewardsCounter.subtract(REWARD_THRESHOLD);
            addCredit(REWARD_CREDIT);
        }
    }
}

