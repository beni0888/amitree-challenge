package com.jbenito.amitree.unit.application.user.get;

import com.jbenito.amitree.application.user.get.GetUser;
import com.jbenito.amitree.domain.model.user.entity.User;
import com.jbenito.amitree.domain.model.user.entity.UserRepository;
import com.jbenito.amitree.domain.model.user.error.UserNotFound;
import com.jbenito.amitree.domain.model.user.value.UserId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;
import java.util.UUID;

import static com.jbenito.amitree.common.user.utils.UserMother.anyUser;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GetUserTest {

    private GetUser getUser;
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        userRepository = mock(UserRepository.class);
        getUser = new GetUser(userRepository);
    }

    @Test
    void givenUserId_whenExecuted_thenItReturnsTheUser() {
        UserId userId = UserId.of(aUserId());
        when(userRepository.findById(eq(userId))).thenReturn(Optional.of(anyUser()));

        User user = getUser.execute(userId);

        assertThat(user).isNotNull();
    }

    @Test
    void givenNonExistentUserId_whenExecuted_thenItThrowsAnException() {
        UserId userId = UserId.of(aUserId());
        when(userRepository.findById(any())).thenReturn(Optional.empty());

        UserNotFound error = assertThrows(UserNotFound.class, () -> getUser.execute(userId));
        assertThat(error.getMessage()).contains("User not found:").contains(userId.getValue().toString());
    }

    private String aUserId() {
        return UUID.randomUUID().toString();
    }

}