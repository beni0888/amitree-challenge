package com.jbenito.amitree.domain.model.user.error;

import com.jbenito.amitree.domain.model.user.value.UserId;

import static java.lang.String.format;

public class UserNotFound extends RuntimeException {
    public UserNotFound(UserId userId) {
        super(format("User not found: [%s]", userId.getValue()));
    }
}
