package com.jbenito.amitree.infrastructure.data;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JpaUserCrudRepository extends JpaRepository<JpaUser, String> {
    Optional<JpaUser> findOneByReferralCode(String referralCode);
    Optional<JpaUser> findOneByEmail(String email);
}