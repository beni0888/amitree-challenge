package com.jbenito.amitree.e2e;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jbenito.amitree.infrastructure.data.JpaReferral;
import com.jbenito.amitree.infrastructure.data.JpaUser;
import com.jbenito.amitree.infrastructure.rest.dto.ErrorResponse;
import com.jbenito.amitree.infrastructure.rest.dto.NewUser;
import com.jbenito.amitree.infrastructure.rest.dto.UserResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.EntityManager;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(properties = "amitree.application.datastore.type=JPA")
@AutoConfigureMockMvc
@Slf4j
public class ApiTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper jsonMapper;
    @Autowired
    private EntityManager entityManager;
    @Autowired
    private TransactionTemplate transactionTemplate;

    @BeforeEach
    @Transactional
    void setUp() {
        transactionTemplate.execute(transactionStatus -> {
            entityManager.createQuery("DELETE from JpaUser").executeUpdate();
            entityManager.createQuery("DELETE from JpaReferral").executeUpdate();
            transactionStatus.flush();
            return null;
        });
    }

    @Test
    void whenCreateUser_thenItWorks() throws Exception {
        NewUser newUser = aNewUser();

        MvcResult result = mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(toJson(newUser))
        )
                .andExpect(status().isCreated())
                .andReturn();

        JsonNode response = jsonMapper.readTree(result.getResponse().getContentAsString());

        assertThat(response.get("id").asText()).isNotEmpty();
        assertThat(response.get("email").asText()).isEqualTo(newUser.email);
        assertThat(response.get("credit").asInt()).isEqualTo(0);
        assertThat(response.get("rewardsCounter").asInt()).isEqualTo(0);
        assertThat(response.get("referralCode").asText()).isNotEmpty();
    }

    @Test
    void whenCreateReferredUser_thenTheReferredUserGetHisCreditAndTheReferrerUserGetsHisReward() throws Exception {
        UserResponse referrerUser = createUser("john.doe@amitree.com");
        UserResponse targetUser = createUser("jane.doe@amitree.com", referrerUser.referralCode);
        referrerUser = getUser(referrerUser);

        assertThat(targetUser.credit).isEqualTo(10);
        assertThat(referrerUser.rewardsCounter).isEqualTo(1);
    }

    @Test
    void whenUserReachesFiveReferredUsers_thenHeGetsTenDollarsInCredit() throws Exception {
        UserResponse referrerUser = createUser("john.doe@amitree.com");
        createUsersConcurrently(5, referrerUser.referralCode);
        referrerUser = getUser(referrerUser);

        assertThat(referrerUser.rewardsCounter).isEqualTo(0);
        assertThat(referrerUser.credit).isEqualTo(10);
    }

    @Test
    void whenCreateUserWithEmptyPayload_thenBadRequestResponseIsReturned() throws Exception {
        MvcResult result = mockMvc.perform(post("/user").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        ErrorResponse response = jsonMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);

        assertThat(response.status).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.message).contains("Required request body is missing");
    }

    @Test
    void whenCreateUserWithInvalidEmail_thenBadRequestResponseIsReturned() throws Exception {
        MvcResult result = mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(aNewUserWith("invalid-email"))))
                .andExpect(status().isBadRequest())
                .andReturn();

        ErrorResponse response = jsonMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);

        assertThat(response.status).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.message).isEqualTo("Invalid email address: [invalid-email]");
    }

    @Test
    void whenCreateUserWithDuplicatedEmail_thenBadRequestResponseIsReturned() throws Exception {
        createUser("john.doe@amitree.com");

        MvcResult result = mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(aNewUserWith("john.doe@amitree.com"))))
                .andExpect(status().isBadRequest())
                .andReturn();

        ErrorResponse response = jsonMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);

        assertThat(response.status).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.message).isEqualTo("User email already exists: [john.doe@amitree.com]");
    }

    @Test
    void whenCreateUserWithNonExistentReferralCode_thenBadRequestResponseIsReturned() throws Exception {
        MvcResult result = mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(aNewUserWith("john.doe@amitree.com", "some-non-existent-code"))))
                .andExpect(status().isBadRequest())
                .andReturn();

        ErrorResponse response = jsonMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);

        assertThat(response.status).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.message).isEqualTo("The referral code does not belong to any existent user: [some-non-existent-code]");
    }

    @Test
    void whenCreateUserWithAlreadyUsedReferralCode_thenBadRequestResponseIsReturned() throws Exception {
        String targetUserEmail = "jane.doe@amitree.com";
        String referralCode = "1234-asdf";

        transactionTemplate.execute(transactionStatus -> {
            entityManager.persist(new JpaUser(UUID.randomUUID().toString(), "john.doe@amitree.com", 0, 0, referralCode));
            entityManager.persist(new JpaReferral(targetUserEmail, referralCode));
            return null;
        });

        MvcResult result = mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(aNewUserWith(targetUserEmail, referralCode))))
                .andExpect(status().isBadRequest())
                .andReturn();

        ErrorResponse response = jsonMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);

        assertThat(response.status).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.message).isEqualTo("The referral code has been already used by this user: [jane.doe@amitree.com, 1234-asdf]");
    }

    @Test
    void whenGetUser_thenItWorks() throws Exception {
        String userEmail = "john.doe@amitree.com";
        UserResponse user = createUser(userEmail);

        MvcResult result = mockMvc.perform(get("/user/{1}", user.id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        UserResponse response = jsonMapper.readValue(result.getResponse().getContentAsString(), UserResponse.class);

        assertThat(response.id).isNotEmpty();
        assertThat(response.email).isEqualTo(userEmail);
        assertThat(response.credit).isEqualTo(0);
        assertThat(response.rewardsCounter).isEqualTo(0);
        assertThat(response.referralCode).isNotEmpty();
    }

    @Test
    void whenGetNonExistentUser_thenNotFoundResponseIsReturned() throws Exception {
        String userId = UUID.randomUUID().toString();
        MvcResult result = mockMvc.perform(get("/user/{1}", userId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn();

        ErrorResponse response = jsonMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);

        assertThat(response.status).isEqualTo(HttpStatus.NOT_FOUND.value());
        assertThat(response.message).contains("User not found:").contains(userId);
    }

    @Test
    void whenGetUserWithInvalidId_thenBadRequestResponseIsReturned() throws Exception {
        String invalidUserId = "1234-asdf";
        MvcResult result = mockMvc.perform(get("/user/{1}", invalidUserId)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();

        ErrorResponse response = jsonMapper.readValue(result.getResponse().getContentAsString(), ErrorResponse.class);

        assertThat(response.status).isEqualTo(HttpStatus.BAD_REQUEST.value());
        assertThat(response.message).contains("Invalid user id:").contains(invalidUserId);
    }

    private UserResponse getUser(UserResponse userId) throws Exception {
        MvcResult result;
        result = mockMvc.perform(get("/user/{id}", userId.id)).andReturn();
        userId = jsonMapper.readValue(result.getResponse().getContentAsString(), UserResponse.class);
        return userId;
    }

    private UserResponse createUser(String email, String referralCode) throws Exception {
        MvcResult result = mockMvc.perform(post("/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(aNewUserWith(email, referralCode)))
        ).andReturn();
        return jsonMapper.readValue(result.getResponse().getContentAsString(), UserResponse.class);
    }

    private UserResponse createUser(String email) throws Exception {
        return createUser(email, null);
    }

    private void createUsersConcurrently(int numberOfUsers, String referralCode) throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(10);

        for (int i = 0; i < numberOfUsers; i++) {
            int emailSeed = i;
            executor.submit(() -> {
                try {
                    mockMvc.perform(post("/user")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(toJson(aNewUserWith(anEmailAddress(emailSeed), referralCode)))
                    ).andReturn();
                } catch (Exception e) {
                    log.error("Error creating user", e);
                }
            });
        }

        executor.shutdown();
        executor.awaitTermination(numberOfUsers, TimeUnit.SECONDS);
    }

    private String anEmailAddress(int emailSeed) {
        return format("sample.user-%d@amitree.com", emailSeed);
    }

    private NewUser aNewUserWith(String email, String referralCode) {
        return new NewUser(email, referralCode);
    }

    private NewUser aNewUserWith(String email) {
        return aNewUserWith(email, null);
    }

    private NewUser aNewUser() {
        return aNewUserWith("john.doe@amitree.com");
    }

    private String toJson(NewUser user) throws JsonProcessingException {
        return jsonMapper.writeValueAsString(user);
    }
}
