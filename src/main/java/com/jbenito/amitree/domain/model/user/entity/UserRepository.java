package com.jbenito.amitree.domain.model.user.entity;

import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;
import com.jbenito.amitree.domain.model.user.value.UserId;

import java.util.Optional;

public interface UserRepository {

    Optional<User> findByEmail(EmailAddress email);

    Optional<User> findByReferralCode(ReferralCode referralCode);

    Optional<User> findById(UserId userId);

    void save(User user);
}
