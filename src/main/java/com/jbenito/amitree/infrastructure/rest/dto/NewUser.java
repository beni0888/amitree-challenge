package com.jbenito.amitree.infrastructure.rest.dto;

import lombok.Value;

@Value
public final class NewUser {
    public final String email;
    public final String referralCode;
}
