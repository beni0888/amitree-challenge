package com.jbenito.amitree.domain.model.user.error;

import static java.lang.String.format;

public class InvalidUserId extends RuntimeException {
    public InvalidUserId(String value) {
        super(format("Invalid user id: [%s]", value));
    }
}
