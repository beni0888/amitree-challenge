package com.jbenito.amitree.integration;

import com.jbenito.amitree.application.user.create.CreateUser;
import com.jbenito.amitree.application.user.create.CreateUserCommand;
import com.jbenito.amitree.application.user.get.GetUser;
import com.jbenito.amitree.application.user.referral.RegisterReferredUser;
import com.jbenito.amitree.common.user.utils.UserMother;
import com.jbenito.amitree.domain.model.user.entity.ReferralRepository;
import com.jbenito.amitree.domain.model.user.entity.User;
import com.jbenito.amitree.domain.model.user.entity.UserRepository;
import com.jbenito.amitree.domain.model.user.value.Credit;
import com.jbenito.amitree.domain.model.user.value.RewardsCounter;
import com.jbenito.amitree.infrastructure.data.InMemoryReferralRepository;
import com.jbenito.amitree.infrastructure.data.InMemoryUserRepository;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

public class CreateUserIntegrationTest {

    private UserRepository userRepository = new InMemoryUserRepository();
    private ReferralRepository referralRepository = new InMemoryReferralRepository();
    private RegisterReferredUser registerReferredUser = new RegisterReferredUser(referralRepository, userRepository);
    private CreateUser createUser = new CreateUser(userRepository, referralRepository, registerReferredUser);
    private GetUser getUser = new GetUser(userRepository);

    @Test
    void createUser_shouldWorksProperlyUnderConcurrentExecution() throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(10);
        User referrerUser = UserMother.anyUser();
        String referralCode = referrerUser.getReferralCode().getValue();
        userRepository.save(referrerUser);

        for (int i = 0; i < 1000; i++) {
            int emailSeed = i;
            executor.submit(() -> createUser.execute(new CreateUserCommand(anEmailAddress(emailSeed), referralCode)));
        }

        executor.shutdown();
        executor.awaitTermination(10, TimeUnit.SECONDS);

        referrerUser = getUser.execute(referrerUser.getId());

        assertThat(referrerUser.getRewardsCounter()).isEqualTo(RewardsCounter.ZERO);
        assertThat(referrerUser.getCredit()).isEqualTo(Credit.of(2000));
    }

    private String anEmailAddress(int seed) {
        return format("sample.email.%d@amitree.com", seed);
    }


}
