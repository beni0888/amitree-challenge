package com.jbenito.amitree.domain.model.user.value;

import com.jbenito.amitree.domain.model.user.error.InvalidUserId;
import lombok.NonNull;
import lombok.Value;

import java.util.UUID;

import static java.util.UUID.randomUUID;

@Value()
public class UserId {
    private UUID value;

    public static UserId of(@NonNull String value) {
        return new UserId(parseUUID(value));
    }

    private static UUID parseUUID(String value) {
        try {
            return UUID.fromString(value);
        } catch (Exception e) {
            throw new InvalidUserId(value);
        }
    }

    public static UserId next() {
        return UserId.of(randomUUID().toString());
    }
}
