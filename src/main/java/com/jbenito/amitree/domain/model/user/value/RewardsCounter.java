package com.jbenito.amitree.domain.model.user.value;

import lombok.Value;

@Value()
public class RewardsCounter {
    public static final RewardsCounter ZERO = RewardsCounter.of(0);

    private final int value;

    public static RewardsCounter of(int value) {
        return new RewardsCounter(value);
    }

    public RewardsCounter increment() {
        return RewardsCounter.of(value + 1);
    }

    public RewardsCounter subtract(RewardsCounter other) {
        return RewardsCounter.of(value - other.value);
    }
    
    public boolean isGreaterThanOrEqual(RewardsCounter other) {
        return this.value >= other.value;
    }

    /**
     * Overridden to actually look at the AtomicInteger inner value when checking for equality
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RewardsCounter that = (RewardsCounter) o;
        return value == that.value;
    }
}
