package com.jbenito.amitree.domain.model.user.error;

import com.jbenito.amitree.domain.model.user.value.ReferralCode;

import static java.lang.String.format;

public class NonExistentReferralCode extends RuntimeException {
    public NonExistentReferralCode(ReferralCode referralCode) {
        super(format("The referral code does not belong to any existent user: [%s]", referralCode.getValue()));
    }
}
