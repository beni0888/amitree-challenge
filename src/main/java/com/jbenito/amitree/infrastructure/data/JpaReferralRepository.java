package com.jbenito.amitree.infrastructure.data;

import com.jbenito.amitree.domain.model.user.entity.Referral;
import com.jbenito.amitree.domain.model.user.entity.ReferralRepository;
import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@ConditionalOnProperty(name = "amitree.application.datastore.type", havingValue = "JPA")
public class JpaReferralRepository implements ReferralRepository {

    private JpaReferralCrudRepository crudRepository;

    public JpaReferralRepository(JpaReferralCrudRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @Override
    public Optional<Referral> findByEmailAndCode(EmailAddress email, ReferralCode referral) {
        return crudRepository.findOneByEmailAndCode(email.getValue(), referral.getValue())
                .flatMap(JpaReferralRepository::toDomainReferral);
    }

    @Override
    public void save(Referral referral) {
        crudRepository.saveAndFlush(JpaReferral.fromDomain(referral));
    }

    private static Optional<Referral> toDomainReferral(JpaReferral jpaReferral) {
        return Optional.of(JpaReferral.toDomain(jpaReferral));
    }
}
