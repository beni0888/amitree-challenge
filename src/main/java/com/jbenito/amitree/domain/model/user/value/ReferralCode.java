package com.jbenito.amitree.domain.model.user.value;

import com.jbenito.amitree.domain.model.user.service.UniqueCodeGenerator;
import lombok.NonNull;
import lombok.Value;

@Value(staticConstructor = "of")
public class ReferralCode {
    @NonNull
    private final String value;

    public static ReferralCode create() {
        return ReferralCode.of(UniqueCodeGenerator.get());
    }
}
