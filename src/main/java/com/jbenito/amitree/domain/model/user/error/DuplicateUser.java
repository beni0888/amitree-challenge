package com.jbenito.amitree.domain.model.user.error;

import com.jbenito.amitree.domain.model.user.value.EmailAddress;

import static java.lang.String.format;

public class DuplicateUser extends RuntimeException {

    public DuplicateUser(EmailAddress email) {
        super(format("User email already exists: [%s]", email.getValue()));
    }
}
