FROM openjdk:8-jdk-alpine

EXPOSE 8080

ADD target/amitree-referral-api-0.0.1-SNAPSHOT.jar app.jar
 
ENTRYPOINT ["java","-Xmx256m","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
