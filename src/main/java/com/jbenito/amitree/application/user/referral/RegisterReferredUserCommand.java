package com.jbenito.amitree.application.user.referral;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@AllArgsConstructor
@EqualsAndHashCode
public class RegisterReferredUserCommand {
    public final String email;
    public final String referralCode;

    public static RegisterReferredUserCommand of(String email, String referralCode) {
        return new RegisterReferredUserCommand(email, referralCode);
    }
}
