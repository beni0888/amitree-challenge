package com.jbenito.amitree.common.user.utils;

import com.jbenito.amitree.domain.model.user.entity.User;
import com.jbenito.amitree.domain.model.user.value.Credit;
import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.RewardsCounter;
import com.jbenito.amitree.domain.model.user.value.UserId;

public class UserMother {

    public static User anyUser() {
        return User.builder()
                .id(UserId.next())
                .email(EmailAddress.of("john.doe@amitree.com"))
                .credit(Credit.ZERO)
                .rewardsCounter(RewardsCounter.ZERO)
                .build();
    }

    public static User anyUserWithRewardsCounterOf(int value) {
        return User.builder()
                .id(UserId.next())
                .email(EmailAddress.of("john.doe@amitree.com"))
                .credit(Credit.ZERO)
                .rewardsCounter(RewardsCounter.of(value))
                .build();
    }

    public static User aUserWithEmail(String email) {
        return User.builder()
                .id(UserId.next())
                .email(EmailAddress.of(email))
                .credit(Credit.ZERO)
                .rewardsCounter(RewardsCounter.ZERO)
                .build();
    }
}