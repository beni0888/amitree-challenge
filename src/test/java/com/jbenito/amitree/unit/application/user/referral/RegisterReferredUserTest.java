package com.jbenito.amitree.unit.application.user.referral;

import com.jbenito.amitree.application.user.referral.RegisterReferredUser;
import com.jbenito.amitree.application.user.referral.RegisterReferredUserCommand;
import com.jbenito.amitree.domain.model.user.entity.Referral;
import com.jbenito.amitree.domain.model.user.entity.ReferralRepository;
import com.jbenito.amitree.domain.model.user.entity.User;
import com.jbenito.amitree.domain.model.user.entity.UserRepository;
import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static com.jbenito.amitree.common.user.utils.UserMother.anyUserWithRewardsCounterOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class RegisterReferredUserTest {

    private RegisterReferredUserCommand command;
    private ReferralRepository referralRepository;
    private RegisterReferredUser registerReferredUser;
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        userRepository = mock((UserRepository.class));
        referralRepository = mock(ReferralRepository.class);
        registerReferredUser = new RegisterReferredUser(referralRepository, userRepository);
        command = null;
    }

    @AfterEach
    void tearDown() {
        reset(userRepository, referralRepository);
    }

    @Test
    void givenRegisterReferredUserCommand_whenExecuted_thenReferralIsRegistered() {
        command = registerReferredUserCommand();
        User user = anyUserWithRewardsCounterOf(1);
        Referral referral = referralFrom(command);

        when(userRepository.findByReferralCode(eq(referralCodeFrom(command))))
                .thenReturn(Optional.of(user));

        registerReferredUser.execute(command);

        assertThat(rewardsCounterFrom(user)).isEqualTo(2);
        verify(referralRepository, times(1)).save(eq(referral));
        verify(userRepository, times(1)).save(eq(user));
    }

    private int rewardsCounterFrom(User user) {
        return user.getRewardsCounter().getValue();
    }

    @Test
    void givenRegisterReferredUserCommand_shouldAddTenToCreditWhenRewardsCounterReachesFive() {
        command = registerReferredUserCommand();
        User user = anyUserWithRewardsCounterOf(4);

        when(userRepository.findByReferralCode(eq(referralCodeFrom(command))))
                .thenReturn(Optional.of(user));

        registerReferredUser.execute(command);

        assertThat(rewardsCounterFrom(user)).isEqualTo(0);
        assertThat(creditFrom(user)).isEqualTo(10);
    }

    private int creditFrom(User user) {
        return user.getCredit().getValue();
    }

    private Referral referralFrom(RegisterReferredUserCommand command) {
        return Referral.of(emailFrom(command), referralCodeFrom(command));
    }

    private ReferralCode referralCodeFrom(RegisterReferredUserCommand command) {
        return ReferralCode.of(command.referralCode);
    }

    private EmailAddress emailFrom(RegisterReferredUserCommand command) {
        return EmailAddress.of(command.email);
    }

    private RegisterReferredUserCommand registerReferredUserCommand() {
        return new RegisterReferredUserCommand("john.doe@amitree.com", "1234asdf");
    }
}