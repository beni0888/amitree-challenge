package com.jbenito.amitree.domain.model.user.error;

import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;

import static java.lang.String.format;

public class ReferralCodeAlreadyUsed extends RuntimeException {
    public ReferralCodeAlreadyUsed(EmailAddress email, ReferralCode referralCode) {
        super(format("The referral code has been already used by this user: [%s, %s]", email.getValue(), referralCode.getValue()));
    }
}
