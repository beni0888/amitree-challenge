package com.jbenito.amitree.application.user.create;

import com.jbenito.amitree.application.user.referral.RegisterReferredUser;
import com.jbenito.amitree.application.user.referral.RegisterReferredUserCommand;
import com.jbenito.amitree.domain.model.user.entity.ReferralRepository;
import com.jbenito.amitree.domain.model.user.entity.User;
import com.jbenito.amitree.domain.model.user.entity.UserRepository;
import com.jbenito.amitree.domain.model.user.error.DuplicateUser;
import com.jbenito.amitree.domain.model.user.error.NonExistentReferralCode;
import com.jbenito.amitree.domain.model.user.error.ReferralCodeAlreadyUsed;
import com.jbenito.amitree.domain.model.user.value.Credit;
import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;
import com.jbenito.amitree.domain.model.user.value.RewardsCounter;

import static com.jbenito.amitree.domain.model.user.value.EmailAddress.of;
import static com.jbenito.amitree.domain.model.user.value.UserId.next;

public final class CreateUser {

    private final UserRepository userRepository;
    private final ReferralRepository referralRepository;
    private final RegisterReferredUser registerReferredUser;

    public CreateUser(UserRepository userRepository, ReferralRepository referralRepository, RegisterReferredUser registerReferredUser) {
        this.userRepository = userRepository;
        this.referralRepository = referralRepository;
        this.registerReferredUser = registerReferredUser;
    }

    public synchronized User execute(CreateUserCommand command) {
        EmailAddress emailAddress = of(command.email);
        assertEmailIsUnique(emailAddress);

        User user = buildUser(emailAddress);

        if (hasReferralCode(command)) {
            assertReferralCodeIsValid(ReferralCode.of(command.referralCode), emailAddress);
            user.addCredit(User.REWARD_CREDIT);
            registerReferredUser.execute(RegisterReferredUserCommand.of(command.email, command.referralCode));
        }

        userRepository.save(user);
        return user;
    }

    private void assertReferralCodeIsValid(ReferralCode referralCode, EmailAddress email) {
        if (!userRepository.findByReferralCode(referralCode).isPresent()) {
            throw new NonExistentReferralCode(referralCode);
        }
        if (referralRepository.findByEmailAndCode(email, referralCode).isPresent()) {
            throw new ReferralCodeAlreadyUsed(email, referralCode);
        }
    }

    private boolean hasReferralCode(CreateUserCommand command) {
        return command.referralCode != null && !command.referralCode.trim().isEmpty();
    }

    private User buildUser(EmailAddress emailAddress) {
        return User.builder()
                    .id(next())
                    .email(emailAddress)
                    .credit(Credit.ZERO)
                    .rewardsCounter(RewardsCounter.ZERO)
                    .build();
    }

    private void assertEmailIsUnique(EmailAddress emailAddress) {
        if (userRepository.findByEmail(emailAddress).isPresent()) {
            throw new DuplicateUser(emailAddress);
        }
    }
}
