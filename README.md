# Amitree take-home exercise
This my solution for the Amitree take-home exercise.

## Considerations

This web application has been developed using the Java programming language, Spring Boot, Spring MVC, Spring Data JPA
and Java Lombok library. I have also used docker to make extremely easy to run the system.

I have use the OOP paradigm for the development and also have followed the hexagonal architecture in order to provide a 
clean architecture with the business logic decoupled from infrastructure concerns. The system has a pretty high code 
coverage (100% class, 90% method and 93% lines) and I can tell that it is almost bug-free (you can never be 100% that your code is bug free).

## Running

In order to run the applications you need to have Java 8 SDK installed on your system. I have added a dockerfile to build a docker 
image which contains the application and every dependency it may need. You need to have docker installed on your system 
in order to build the docker image and run the application through a docker container.

### Run without docker

You can easily run the system manually without docker, I have added a maven wrapper to ease the build and run. You can 
the app using the spring boot maven plugin `./mvnw spring-boot:run` or executing the `jar` file generated after running 
`./mvnw package`.

**With Spring Boot maven plugin:**

```bash
> cd /path/to/application
> ./mvnw spring-boot:run
```  

**With vanilla Java:**
```bash
> cd /path/to/application
> ./mvnw package
> java -jar target/amitree-referral-api-0.0.1-SNAPSHOT.jar
```

### Run with docker

```bash
> cd /path/to/application
> docker build . -t jbenito/amitree
> docker run -p 8080:8080 --rm jbenito/amitree
```

If you don't want to build the docker image by your own, you can download it from docker hub (take into account that in 
this case the image ID is `beni0888/amitree` as my docker hub user is `beni0888`:

```bash
> docker pull beni0888/amitree
> docker run -p 8080:8080 --rm beni0888/amitree
```

### Deployed version

In order to make it even easier for you, I've deployed the application on a Heroku free dyno that can be reached at 
[https://jbenito-amitree.herokuapp.com/](https://jbenito-amitree.herokuapp.com/). Please take into account that it is 
hosted on a Heroku's free dyno server that go asleep after 30-minutes of no-traffic, so the first time you access the 
application you more likely have to wait for a while until it wakes up.

## REST API

The REST API developed as a solution for the given exercise is made by the following endpoints:
 
* POST `/user` Creates a new user.

```bash
> curl -v 'https://jbenito-amitree.herokuapp.com/user' \ 
    -H 'Content-type: application/json' \
    -d '{"email": "john.doe@amitree.com"}'
    
< HTTP/1.1 201
< X-Request-Id: 157947A23D7343E584FEB1D760C40E04
< Content-Type: application/json
{
  "id": "d19fdd1f-9103-4562-9b30-f057151e5db8",
  "email": "john.doe@amitree.com",
  "credit": 0,
  "rewardsCounter": 0,
  "referralCode": "ZmU3NzYyNzktMmI5Ni00MzcxLTk2NDktMDc1ZmJmMzUyODI5"
}    
```

Create referred user:

```bash
> curl -v 'https://jbenito-amitree.herokuapp.com/user' \ 
    -H 'Content-type: application/json' \
    -d '{"email": "jane.doe@amitree.com", "referralCode": "ZmU3NzYyNzktMmI5Ni00MzcxLTk2NDktMDc1ZmJmMzUyODI5"}'
    
< HTTP/1.1 201
< X-Request-Id: 157947A23D7343E584FEB1D760C40E04
< Content-Type: application/json
{
  "id": "511ae04e-58bd-43fd-a668-8f78e87b6cb7",
  "email": "jane.doe@amitree.com",
  "credit": 10,
  "rewardsCounter": 0,
  "referralCode": "ZGU4ZTg1MWUtNmViNi00NjEyLTg4YjMtNzQ2YmIxNDY2NjNk"
}
```

* GET `/user/{userId}` Fetch an existing user 
```bash
> curl -v 'https://jbenito-amitree.herokuapp.com/user/d19fdd1f-9103-4562-9b30-f057151e5db8'
    
< HTTP/1.1 200
< X-Request-Id: DE63ADD436024D42A28486DBC5352BA9
< Content-Type: application/json
{
  "id": "d19fdd1f-9103-4562-9b30-f057151e5db8",
  "email": "john.doe@amitree.com",
  "credit": 0,
  "rewardsCounter": 0,
  "referralCode": "ZmU3NzYyNzktMmI5Ni00MzcxLTk2NDktMDc1ZmJmMzUyODI5"
}
```

### Error handling

The application has got a proper error handling mechanism implemented that take into account the following scenarios:

* Create user with invalid payload (or even without it), then return `400 Bad Request`
* Create user with invalid referral code, then return `400 Bad Request`
* Create user with an already used referral code, then return `400 Bad Request`
* Get user with non-existent ID, then return `404 Not found`

You can see the whole list of covered cases in the test suite of the application, which is its live documentation.

## Assumptions and decisions made

* I've used Lombok library to reduce the boilerplate when creating Java POJOs and common methods like constructors, 
equals and hash code, etc.
* Even though I’ve used Lombok’s `@Value` annotation in multiple classes, which converts every field in a `private final` 
one, I’ve decided to make private final explicit for the sake of clarity and readability for those not used to Lombok.
* I've decided to just use an email field as the user's identifier (apart from the user's id field which is more an internal ID) 
and haven't added any other field like `name`, `surname`, etc. for the sake of brevity.
* I've provided the application with two different persistence mechanisms: in-memory and H2 DB which is also an SQL in-memory DB. 
The application runs by default using the H2 persistence mechanism, although you can easily change that behaviour just by
setting the property `amitree.application.datastore.type=MEM`. The in-memory repositories are being used in some integration 
tests in order to make it easier and faster to test some use cases.
* When checking for already used referrals, I’m looking for email and referral code, but just looking for email should be enough 
as an user shouldn’t be allowed to get referral rewards when registering multiple times (eg. a user is registered using a referral, 
after a while the user request to remove his account and later the user register himself again using a different referral code).
* I've added a correlation-id mechanism that allows to easily track every request-related log trace. You can get its value
for every request from `X-Request-Id` HTTP response header.

## TODO
* The next feature (or enhancement) I would have liked to add is calling `RegisterReferredUser` from `CreateUser` in an 
asynchronous way, by publishing an event in a message broker system. 