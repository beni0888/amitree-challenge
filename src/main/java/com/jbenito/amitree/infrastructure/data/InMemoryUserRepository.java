package com.jbenito.amitree.infrastructure.data;

import com.jbenito.amitree.domain.model.user.entity.User;
import com.jbenito.amitree.domain.model.user.entity.UserRepository;
import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;
import com.jbenito.amitree.domain.model.user.value.UserId;
import lombok.NonNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Repository
@ConditionalOnProperty(name = "amitree.application.datastore.type", havingValue = "MEM", matchIfMissing = true)
public class InMemoryUserRepository implements UserRepository {

    private ConcurrentHashMap<EmailAddress, User> emailIndexedStore = new ConcurrentHashMap<>();
    private ConcurrentHashMap<ReferralCode, User> referralIndexedStore = new ConcurrentHashMap<>();
    private ConcurrentHashMap<UserId, User> idIndexedStore = new ConcurrentHashMap<>();

    private ReadWriteLock lock = new ReentrantReadWriteLock();

    @Override
    public Optional<User> findByEmail(@NonNull EmailAddress email) {
        lock.readLock().lock();
        try {
            User user = emailIndexedStore.get(email);
            return Optional.ofNullable(user != null ? User.of(user) : null);
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public Optional<User> findByReferralCode(@NonNull ReferralCode referralCode) {
        lock.readLock().lock();
        try {
            User user = referralIndexedStore.get(referralCode);
            return Optional.ofNullable(user != null ? User.of(user) : null);
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public Optional<User> findById(@NonNull UserId userId) {
        lock.readLock().lock();
        try {
            User user = idIndexedStore.get(userId);
            return Optional.ofNullable(user != null ? User.of(user) : null);
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public void save(@NonNull User user) {
        lock.writeLock().lock();
        try {
            User userToPersist = User.of(user);
            emailIndexedStore.put(userToPersist.getEmail(), userToPersist);
            referralIndexedStore.put(userToPersist.getReferralCode(), userToPersist);
            idIndexedStore.put(userToPersist.getId(), userToPersist);
        } finally {
            lock.writeLock().unlock();
        }
    }
}
