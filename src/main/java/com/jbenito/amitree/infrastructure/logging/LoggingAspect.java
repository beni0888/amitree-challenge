package com.jbenito.amitree.infrastructure.logging;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class LoggingAspect {

    private static final String MDC_OPERATION_KEY = "LoggingAspect.operation";

    @Pointcut(value = "@annotation(loggable) && execution(* *(..))", argNames = "loggable")
    public void loggableMethod(Loggable loggable) {
        // Empty because it's just a pointcut
    }

    @Around(value = "loggableMethod(loggable)", argNames = "pjp, loggable")
    public Object logAccess(ProceedingJoinPoint pjp, Loggable loggable) throws Throwable {
        try {
            MDC.put(MDC_OPERATION_KEY, loggable.operation());
            log.info("message=\"Start operation\"");
            Object result = result = pjp.proceed();
            logSuccessMessage(loggable, result);
            return result;
        } catch (Throwable throwable) {
            log.error("message=\"Error while executing operation\" error=\"{}\"}", throwable.getMessage(), throwable);
            throw throwable;
        } finally {
            log.info("message=\"End operation\"");
            MDC.remove(MDC_OPERATION_KEY);
        }
    }

    private void logSuccessMessage(Loggable loggable, Object result) {
        if (!loggable.successMessage().isEmpty()) {
            log.info("message=\"{}: {}\"", loggable.successMessage(), result);
        }
    }
}
