package com.jbenito.amitree.infrastructure.data;

import com.jbenito.amitree.domain.model.user.entity.User;
import com.jbenito.amitree.domain.model.user.entity.UserRepository;
import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;
import com.jbenito.amitree.domain.model.user.value.UserId;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@ConditionalOnProperty(name = "amitree.application.datastore.type", havingValue = "JPA")
public class JpaUserRepository implements UserRepository {
    
    private JpaUserCrudRepository crudRepository;

    public JpaUserRepository(JpaUserCrudRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    private static Optional<User> toDomainUser(JpaUser jpaUser) {
        return Optional.of(JpaUser.toDomain(jpaUser));
    }

    @Override
    public Optional<User> findByEmail(EmailAddress email) {
        return crudRepository.findOneByEmail(email.getValue()).flatMap(JpaUserRepository::toDomainUser);
    }

    @Override
    public Optional<User> findByReferralCode(ReferralCode referralCode) {
        String referral = referralCode.getValue();
        return crudRepository.findOneByReferralCode(referral).flatMap(JpaUserRepository::toDomainUser);
    }

    @Override
    public Optional<User> findById(UserId userId) {
        String id = userId.getValue().toString();
        return crudRepository.findById(id).flatMap(JpaUserRepository::toDomainUser);
    }

    @Override
    public void save(User user) {
        crudRepository.saveAndFlush(JpaUser.fromDomain(user));
    }

}
