package com.jbenito.amitree.application.user.create;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public final class CreateUserCommand {
    public final String email;
    public final String referralCode;
}
