package com.jbenito.amitree.unit.infrastructure.data;

import com.jbenito.amitree.common.user.utils.UserMother;
import com.jbenito.amitree.domain.model.user.entity.User;
import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;
import com.jbenito.amitree.domain.model.user.value.RewardsCounter;
import com.jbenito.amitree.infrastructure.data.InMemoryUserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static com.jbenito.amitree.common.user.utils.UserMother.aUserWithEmail;
import static com.jbenito.amitree.common.user.utils.UserMother.anyUserWithRewardsCounterOf;
import static org.assertj.core.api.Assertions.assertThat;

class InMemoryUserRepositoryTest {

    InMemoryUserRepository repository;

    @BeforeEach
    void setUp() {
        repository = new InMemoryUserRepository();
    }

    @Test
    void givenUser_whenSave_thenUserIsAddedToRepository() {
        User user = UserMother.anyUser();

        repository.save(user);

        User retrievedUser = repository.findByEmail(user.getEmail())
                .orElseThrow(() -> new RuntimeException("User not found"));
        assertThat(user).isEqualTo(retrievedUser);
    }

    @Test
    void givenNonExistentEmail_whenFindByEmail_thenNoUserIsReturned() {
        Optional<User> user = repository.findByEmail(EmailAddress.of("non-existen-user@amitree.com"));

        assertThat(user).isEmpty();
    }

    @Test
    void givenNonExistentEmail_whenFindByReferral_thenNoUserIsReturned() {
        Optional<User> user = repository.findByReferralCode(ReferralCode.of("0000111"));

        assertThat(user).isEmpty();
    }

    @Test
    void givenRepositoryWithMultipleUsers_whenFindByEmail_thenTheRightUserIsReturned() {
        repository.save(aUserWithEmail("jesus.benito@amitree.com"));
        repository.save(aUserWithEmail("john.doe@amitree.com"));

        User user = repository.findByEmail(EmailAddress.of("jesus.benito@amitree.com"))
                .orElseThrow(() -> new RuntimeException("User not found"));

        assertThat(user.getEmail()).isEqualTo(EmailAddress.of("jesus.benito@amitree.com"));
    }

    @Test
    void givenRepositoryWithMultipleUsers_whenFindByReferralCode_thenTheRightUserIsReturned() {
        User targetUser = aUserWithEmail("jesus.benito@amitree.com"),
            anotherUser = aUserWithEmail("john.doe@amitree.com");
        repository.save(targetUser);
        repository.save(anotherUser);

        User user = repository.findByReferralCode(targetUser.getReferralCode())
                .orElseThrow(() -> new RuntimeException("User not found"));

        assertThat(targetUser.getReferralCode()).isNotEqualTo(anotherUser.getReferralCode());
        assertThat(user).isEqualTo(targetUser);
    }

    @Test
    void giveUserInRepository_whenUserIsUpdatedAndSaved_thenUserIsUpdatedInRepository() {
        User user = anyUserWithRewardsCounterOf(2);
        repository.save(user);
        RewardsCounter updateRewardsCounter = RewardsCounter.of(4);
        user.setRewardsCounter(updateRewardsCounter);

        repository.save(user);
        User retrievedUser = repository.findByEmail(user.getEmail())
                .orElseThrow(() -> new RuntimeException("User not found"));

        assertThat(retrievedUser).isEqualTo(user);
        assertThat(retrievedUser.getRewardsCounter()).isEqualTo(updateRewardsCounter);

    }
}