package com.jbenito.amitree.domain.model.user.error;

import static java.lang.String.format;

public class InvalidEmailAddress extends RuntimeException {
    public InvalidEmailAddress(String email) {
        super(format("Invalid email address: [%s]", email));
    }
}
