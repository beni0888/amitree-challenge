package com.jbenito.amitree.infrastructure.data;

import com.jbenito.amitree.domain.model.user.entity.Referral;
import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class JpaReferral {
    @Id
    private String email;
    private String code;

    public static JpaReferral fromDomain(@NonNull Referral referral) {
        return new JpaReferral(referral.getEmail().getValue(), referral.getCode().getValue());
    }

    public static Referral toDomain(@NonNull JpaReferral referral) {
        return Referral.of(EmailAddress.of(referral.email), ReferralCode.of(referral.code));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JpaReferral referral = (JpaReferral) o;
        return email.equals(referral.email) &&
                code.equals(referral.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, code);
    }
}
