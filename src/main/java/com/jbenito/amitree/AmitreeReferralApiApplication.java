package com.jbenito.amitree;

import com.jbenito.amitree.application.user.create.CreateUser;
import com.jbenito.amitree.application.user.get.GetUser;
import com.jbenito.amitree.application.user.referral.RegisterReferredUser;
import com.jbenito.amitree.domain.model.user.entity.ReferralRepository;
import com.jbenito.amitree.domain.model.user.entity.UserRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@SpringBootApplication
public class AmitreeReferralApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AmitreeReferralApiApplication.class, args);
    }

    @Bean
    @Transactional
    public RegisterReferredUser registerReferredUserService(ReferralRepository referralRepository,
                                                            UserRepository userRepository) {
        return new RegisterReferredUser(referralRepository, userRepository);
    }

    @Bean
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public CreateUser createUserService(UserRepository userRepository,
                                        ReferralRepository referralRepository,
                                        RegisterReferredUser registerReferredUser) {
        return new CreateUser(userRepository, referralRepository, registerReferredUser);
    }

    @Bean
    public GetUser getUserService(UserRepository userRepository) {
        return new GetUser(userRepository);
    }

}
