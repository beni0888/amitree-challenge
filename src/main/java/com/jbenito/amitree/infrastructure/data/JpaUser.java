package com.jbenito.amitree.infrastructure.data;

import com.jbenito.amitree.domain.model.user.entity.User;
import com.jbenito.amitree.domain.model.user.value.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
public class JpaUser {
    @Id
    private String id;
    private String email;
    private int credit;
    private int rewardsCounter;
    private String referralCode;

    static JpaUser fromDomain(@NonNull User user) {
        return new JpaUser(user.getId().getValue().toString(),
                user.getEmail().getValue(),
                user.getCredit().getValue(),
                user.getRewardsCounter().getValue(),
                user.getReferralCode().getValue()
        );
    }

    static User toDomain(@NonNull JpaUser user) {
        return User.builder()
                .id(UserId.of(user.id))
                .email(EmailAddress.of(user.email))
                .credit(Credit.of(user.credit))
                .rewardsCounter(RewardsCounter.of(user.rewardsCounter))
                .referralCode(ReferralCode.of(user.referralCode))
                .build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JpaUser jpaUser = (JpaUser) o;
        return id.equals(jpaUser.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
