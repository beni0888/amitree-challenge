package com.jbenito.amitree.infrastructure.rest.dto;

import com.jbenito.amitree.domain.model.user.entity.User;
import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public final class UserResponse {
    public final String id;
    public final String email;
    public final int credit;
    public final int rewardsCounter;
    public final String referralCode;

    public static UserResponse fromUser(User user) {
        return new UserResponse(user.getId().getValue().toString(),
                user.getEmail().getValue(),
                user.getCredit().getValue(),
                user.getRewardsCounter().getValue(),
                user.getReferralCode().getValue());
    }
}
