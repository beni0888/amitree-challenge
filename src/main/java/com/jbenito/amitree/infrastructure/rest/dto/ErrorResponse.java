package com.jbenito.amitree.infrastructure.rest.dto;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public final class ErrorResponse {
    public final int status;
    public final String message;
}
