package com.jbenito.amitree.domain.model.user.value;

import com.jbenito.amitree.domain.model.user.error.InvalidEmailAddress;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.Value;
import org.apache.commons.validator.routines.EmailValidator;

@Value()
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class EmailAddress {
    @NonNull
    private String value;

    public static EmailAddress of(String value) {
        assertEmailIsValidEmailAddress(value);
        return new EmailAddress(value);
    }

    private static void assertEmailIsValidEmailAddress(String email) {
        if(!EmailValidator.getInstance().isValid(email)) {
            throw new InvalidEmailAddress(email);
        }
    }
}
