package com.jbenito.amitree.unit.application.user.create;

import com.jbenito.amitree.application.user.create.CreateUser;
import com.jbenito.amitree.application.user.create.CreateUserCommand;
import com.jbenito.amitree.application.user.referral.RegisterReferredUser;
import com.jbenito.amitree.application.user.referral.RegisterReferredUserCommand;
import com.jbenito.amitree.domain.model.user.entity.Referral;
import com.jbenito.amitree.domain.model.user.entity.ReferralRepository;
import com.jbenito.amitree.domain.model.user.entity.User;
import com.jbenito.amitree.domain.model.user.entity.UserRepository;
import com.jbenito.amitree.domain.model.user.error.DuplicateUser;
import com.jbenito.amitree.domain.model.user.error.InvalidEmailAddress;
import com.jbenito.amitree.domain.model.user.error.NonExistentReferralCode;
import com.jbenito.amitree.domain.model.user.error.ReferralCodeAlreadyUsed;
import com.jbenito.amitree.domain.model.user.value.Credit;
import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;
import com.jbenito.amitree.domain.model.user.value.RewardsCounter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Base64;
import java.util.Optional;
import java.util.UUID;

import static com.jbenito.amitree.common.user.utils.UserMother.anyUser;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class CreateUserTest {

    private CreateUser createUser;
    private UserRepository userRepository;
    private CreateUserCommand command;
    private RegisterReferredUser registerReferredUser;
    private ReferralRepository referralRepository;

    @BeforeEach
    void setUp() {
        userRepository = mock(UserRepository.class);
        registerReferredUser = mock(RegisterReferredUser.class);
        referralRepository = mock(ReferralRepository.class);
        createUser = new CreateUser(userRepository, referralRepository, registerReferredUser);
        command = null;
    }

    @Test
    void givenCreateUserCommandWithValidData_whenCreateUserIsExecuted_thenNewUserIsCreated() {
        command = createUserCommandWithoutReferralCode();

        User user = createUser.execute(command);

        verify(userRepository, times(1)).save(eq(user));
        assertThat(user.getId()).isNotNull();
        assertThat(user.getEmail().toString()).contains(command.email);
        assertThat(user.getCredit()).isEqualTo(Credit.ZERO);
        assertThat(user.getRewardsCounter()).isEqualTo(RewardsCounter.ZERO);
    }

    @Test
    void givenCreateUserCommandWithInvalidEmailAddress_whenCreateUserIsExecuted_thenExceptionIsThrown() {
        InvalidEmailAddress error = assertThrows(InvalidEmailAddress.class, () -> {
            createUser.execute(createUserCommandWithInvalidEmail());
        });

        assertThat(error.getMessage()).isEqualTo("Invalid email address: [not-an-email-address]");
    }

    @Test
    void givenCreateUserCommandWithAlreadyExistingUser_whenCreateUserIsExecuted_thenExceptionIsThrown() {
        DuplicateUser error = assertThrows(DuplicateUser.class, () -> {

            command = createUserCommandWithDuplicateUser();
            EmailAddress email = emailFrom(command);
            when(userRepository.findByEmail(eq(email))).thenReturn(Optional.of(anyUser()));

            createUser.execute(command);
        });

        assertThat(error.getMessage()).isEqualTo("User email already exists: [john.doe@amitree.com]");
    }

    @Test
    void givenCreateUserCommandWithReferral_whenCreateUserIsExecuted_thenReferralCreditIsGivenToUser() {
        command = createUserCommandWithReferralCode();
        when(userRepository.findByReferralCode(eq(referralFrom(command))))
                .thenReturn(Optional.of(anyUser()));

        User user = createUser.execute(command);

        assertThat(user.getCredit()).isEqualTo(User.REWARD_CREDIT);
    }

    @Test
    void givenCreateUserCommandWithInvalidReferral_whenCreateUserIsExecuted_thenExceptionIsThrown() {
        NonExistentReferralCode error = assertThrows(NonExistentReferralCode.class, () -> {
            command = createUserCommandWithInvalidReferralCode();
            when(userRepository.findByReferralCode(eq(referralFrom(command))))
                    .thenReturn(Optional.empty());

            createUser.execute(command);
        });

        assertThat(error.getMessage())
                .contains("The referral code does not belong to any existent user: ")
                .contains(command.referralCode);
    }

    @Test
    void givenCreateUserCommandWithAlreadyUsedReferralCode_whenCreateUserIsExecuted_thenExceptionIsThrown() {
        command = createUserCommandWithReferralCodeAlreadyUsed();
        when(userRepository.findByReferralCode(eq(referralFrom(command))))
                .thenReturn(Optional.of(anyUser()));
        when(referralRepository.findByEmailAndCode(eq(emailFrom(command)), eq(referralFrom(command))))
                .thenReturn(Optional.of(anyReferral()));

        ReferralCodeAlreadyUsed error = assertThrows(ReferralCodeAlreadyUsed.class, () -> {
            createUser.execute(command);
        });

        assertThat(error.getMessage())
                .contains("The referral code has been already used by this user:")
                .contains(emailFrom(command).getValue())
                .contains(referralFrom(command).getValue());
    }

    @Test
    void givenCreateUserCommandWithReferral_whenCreateUserIsExecuted_thenReferralIsRegistered() {
        command = createUserCommandWithReferralCode();
        when(userRepository.findByReferralCode(eq(referralFrom(command))))
                .thenReturn(Optional.of(anyUser()));
        when(referralRepository.findByEmailAndCode(eq(emailFrom(command)), eq(referralFrom(command))))
                .thenReturn(Optional.empty());

        createUser.execute(command);

        verify(registerReferredUser, times(1))
                .execute(eq(registerReferredUserCommand(command.email, command.referralCode)));
    }

    private EmailAddress emailFrom(CreateUserCommand command) {
        return EmailAddress.of(command.email);
    }

    private Referral anyReferral() {
        return Referral.of(EmailAddress.of("john.doe@amitree.com"), ReferralCode.of(referralCode()));
    }

    private CreateUserCommand createUserCommandWithReferralCodeAlreadyUsed() {
        return createUserCommandWithReferralCode();
    }

    private ReferralCode referralFrom(CreateUserCommand command) {
        return ReferralCode.of(command.referralCode);
    }

    private RegisterReferredUserCommand registerReferredUserCommand(String email, String referralCode) {
        return new RegisterReferredUserCommand(email, referralCode);
    }

    private CreateUserCommand createUserCommandWithInvalidReferralCode() {
        return createUserCommandWithReferralCode();
    }

    private CreateUserCommand createUserCommandWithReferralCode() {
        return new CreateUserCommand("john.doe@amitree.com", referralCode());
    }

    private String referralCode() {
        return Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
    }

    private CreateUserCommand createUserCommandWithDuplicateUser() {
        return createUserCommandWithoutReferralCode();
    }

    private CreateUserCommand createUserCommandWithInvalidEmail() {
        return new CreateUserCommand("not-an-email-address", null);
    }

    private CreateUserCommand createUserCommandWithoutReferralCode() {
        return new CreateUserCommand("john.doe@amitree.com", null);
    }
}