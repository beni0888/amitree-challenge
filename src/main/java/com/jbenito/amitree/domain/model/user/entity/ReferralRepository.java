package com.jbenito.amitree.domain.model.user.entity;

import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;

import java.util.Optional;

public interface ReferralRepository {

    Optional<Referral> findByEmailAndCode(EmailAddress email, ReferralCode referral);

    void save(Referral referral);
}
