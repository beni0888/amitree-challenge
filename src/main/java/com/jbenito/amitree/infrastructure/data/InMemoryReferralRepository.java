package com.jbenito.amitree.infrastructure.data;

import com.jbenito.amitree.domain.model.user.entity.Referral;
import com.jbenito.amitree.domain.model.user.entity.ReferralRepository;
import com.jbenito.amitree.domain.model.user.value.EmailAddress;
import com.jbenito.amitree.domain.model.user.value.ReferralCode;
import lombok.NonNull;
import lombok.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Repository
@ConditionalOnProperty(name = "amitree.application.datastore.type", havingValue = "MEM", matchIfMissing = true)
public class InMemoryReferralRepository implements ReferralRepository {

    private ConcurrentHashMap<EntryKey, Referral> store = new ConcurrentHashMap<>();

    private ReadWriteLock lock = new ReentrantReadWriteLock();

    @Override
    public Optional<Referral> findByEmailAndCode(EmailAddress email, ReferralCode referral) {
        lock.readLock().lock();
        try {
            return Optional.ofNullable(store.get(EntryKey.of(email, referral)));
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public void save(Referral referral) {
        lock.writeLock().lock();
        try {
            EntryKey key = EntryKey.of(referral);
            store.put(key, referral);
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Value(staticConstructor = "of")
    private static class EntryKey {
        @NonNull String value;

        public static EntryKey of(Referral referral) {
            return of(referral.getEmail(), referral.getCode());
        }

        public static EntryKey of(EmailAddress emailAddress, ReferralCode referralCode) {
            return of(emailAddress.getValue() + "-" + referralCode.getValue());
        }
    }
}
